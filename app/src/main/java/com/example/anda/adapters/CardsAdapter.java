package com.example.anda.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.alespero.expandablecardview.ExpandableCardView;
import com.example.anda.R;
import com.example.anda.model.ExpandableCardInfo;

import java.util.ArrayList;

public class CardsAdapter extends RecyclerView.Adapter<CardsAdapter.ViewHolder>
{
    private ArrayList<ExpandableCardInfo> cards;

    public CardsAdapter(ArrayList<ExpandableCardInfo> cards)
    {
        this.cards = cards;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.cards_container, parent, false);
        return new ViewHolder(view);
    }

    public void onBindViewHolder(ViewHolder viewHolder, int position)
    {
        viewHolder.expandableCardView.setTitle(cards.get(position).getTitle());
        viewHolder.expandableCardView.setIcon(cards.get(position).getIcon());
        viewHolder.innerText.setText(cards.get(position).getInfo());
    }

    public int getItemCount()
    {
        return cards.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        ExpandableCardView expandableCardView;
        TextView innerText;

        public ViewHolder(View itemView)
        {
            super(itemView);
            expandableCardView = itemView.findViewById(R.id.expandable_card);
            innerText = itemView.findViewById(R.id.expanded_card_home);
        }
    }
}
