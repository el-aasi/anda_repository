package com.example.anda.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.anda.MainActivity;
import com.example.anda.R;
import com.example.anda.model.Hero;


import java.util.ArrayList;


public class HeroAdapter extends RecyclerView.Adapter<HeroAdapter.ViewHolder>
{
    private ArrayList<Hero> heroes;
    private Context context;

    public HeroAdapter(Context context, ArrayList<Hero> heroes)
    {
        this.context = context;
        this.heroes= heroes;
    }

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.hero_container, parent, false);
        return new ViewHolder(view);
    }

    public void onBindViewHolder(ViewHolder viewHolder, int position)
    {
        viewHolder.name.setText(heroes.get(position).getLocalized_name());

        switch(heroes.get(position).getPrimary_attr()) {
            case "str":
                viewHolder.primaryAttribute.setText("Strength");
                break;
            case "agi":
                viewHolder.primaryAttribute.setText("Agility");
                break;
            case "int":
                viewHolder.primaryAttribute.setText("Intelligence");
                break;
            default:
                viewHolder.primaryAttribute.setText("Did you just invent a new attribute?");
        }

        viewHolder.attackType.setText(heroes.get(position).getAttack_type());

        String imgUrl = "https://api.opendota.com" + heroes.get(position).getImageURL();

        Glide.with(context)
                .load(imgUrl)
                .placeholder(R.drawable.ic_anda_foreground)
                .into(viewHolder.imageView);
    }

    public int getItemCount()
    {
        return heroes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView name;
        TextView primaryAttribute;
        TextView attackType;
        ImageView imageView;

        ViewHolder(View itemView)
        {
            super(itemView);
            name = itemView.findViewById(R.id.hero_name);
            primaryAttribute = itemView.findViewById(R.id.hero_primary_attr);
            attackType = itemView.findViewById(R.id.hero_attack_type);
            imageView = itemView.findViewById(R.id.heroIcon);
        }
    }
}
