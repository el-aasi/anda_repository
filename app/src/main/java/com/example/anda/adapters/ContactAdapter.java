package com.example.anda.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.anda.R;
import com.example.anda.model.Contact;

import java.util.ArrayList;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder>
{
    private ArrayList<Contact> contacts;

    public ContactAdapter(ArrayList<Contact> contacts)
    {
        this.contacts = contacts;
    }

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.contacts_container, parent, false);
        return new ViewHolder(view);
    }

    public void onBindViewHolder(ViewHolder viewHolder, int position)
    {
        viewHolder.firstName.setText(contacts.get(position).getFirstName());
        viewHolder.lastName.setText(contacts.get(position).getLastName());
        viewHolder.email.setText(contacts.get(position).getEmail());
        viewHolder.phone.setText(contacts.get(position).getPhone());
    }

    public int getItemCount()
    {
        return contacts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView firstName;
        TextView lastName;
        TextView email;
        TextView phone;

        ViewHolder(View itemView)
        {
            super(itemView);
            firstName = itemView.findViewById(R.id.contact_first_name);
            lastName = itemView.findViewById(R.id.contact_last_name);
            email = itemView.findViewById(R.id.contact_email);
            phone = itemView.findViewById(R.id.contact_phone);
        }
    }
}
