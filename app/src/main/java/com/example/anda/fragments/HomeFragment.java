package com.example.anda.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.anda.R;
import com.example.anda.adapters.CardsAdapter;
import com.example.anda.model.ExpandableCardInfo;
import com.example.anda.viewmodel.CardViewModel;

import java.util.ArrayList;


public class HomeFragment extends Fragment
{
    private CardsAdapter cardsAdapter;
    private RecyclerView cardRecyclerList;
    private ArrayList<ExpandableCardInfo> cardsList;
    private CardViewModel cardViewModel;

    public HomeFragment()
    {

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        cardViewModel = new ViewModelProvider(this).get(CardViewModel.class);
        cardsList = cardViewModel.getAllCards();
        cardsAdapter = new CardsAdapter(cardsList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
       View view = inflater.inflate(R.layout.fragment_home, container, false);

        cardRecyclerList = view.findViewById(R.id.recycler_view);
        cardRecyclerList.setAdapter(cardsAdapter);
        cardRecyclerList.setLayoutManager(new LinearLayoutManager(view.getContext()));

        return view;
    }

}
