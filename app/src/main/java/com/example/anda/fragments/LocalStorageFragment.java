package com.example.anda.fragments;

import android.content.res.Configuration;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.example.anda.R;
import com.example.anda.adapters.ContactAdapter;
import com.example.anda.model.Contact;
import com.example.anda.viewmodel.ContactsViewModel;

import java.util.ArrayList;

public class LocalStorageFragment extends Fragment
{
    private ContactsViewModel contactsViewModel;
    private RecyclerView contactsList;
    private ContactAdapter contactsAdapter;

    public LocalStorageFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        contactsViewModel = new ViewModelProvider(this).get(ContactsViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.fragment_local_storage, container, false);

        contactsViewModel.getAllContacts().observe(getViewLifecycleOwner(), contacts ->
        {
            contactsAdapter = new ContactAdapter(new ArrayList<>(contacts));
            contactsList.setAdapter(contactsAdapter);
        });

        contactsList = view.findViewById(R.id.recycler_view);
        contactsList.hasFixedSize();
        contactsList.setLayoutManager(new LinearLayoutManager(view.getContext()));

        Button saveButton = view.findViewById(R.id.save_button);

        saveButton.setOnClickListener(x ->
        {
            EditText firstName = view.findViewById(R.id.firstName);
            EditText lastName = view.findViewById(R.id.lastName);
            EditText email = view.findViewById(R.id.email);
            EditText phone = view.findViewById(R.id.phone);

            contactsViewModel.insert(new Contact(firstName.getText().toString(),
                    lastName.getText().toString(),
                    email.getText().toString(),
                    phone.getText().toString()));

            firstName.setText("");
            lastName.setText("");
            email.setText("");
            phone.setText("");
        });

        Button deleteAllButton = view.findViewById(R.id.delete_button);

        deleteAllButton.setOnClickListener(x -> contactsViewModel.deleteAllContacts());

        return view;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);

        if(getActivity().getSupportFragmentManager() != null)
        {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .detach(this)
                    .attach(this)
                    .commit();
        }
    }
}
