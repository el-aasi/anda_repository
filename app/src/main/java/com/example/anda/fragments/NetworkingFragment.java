package com.example.anda.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.anda.R;
import com.example.anda.adapters.HeroAdapter;
import com.example.anda.model.Hero;
import com.example.anda.viewmodel.HeroViewModel;
import java.util.ArrayList;
import java.util.List;

public class NetworkingFragment extends Fragment
{
    private HeroViewModel heroViewModel;
    private RecyclerView heroList;
    private HeroAdapter heroAdapter;

    public NetworkingFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        heroViewModel = new ViewModelProvider(this).get(HeroViewModel.class);
        LiveData<List<Hero>> heroLiveList = heroViewModel.getHeroes();
        heroLiveList.observe(this, heroes ->
        {
            heroAdapter = new HeroAdapter(getActivity(), new ArrayList<Hero>(heroes));
            heroList.setAdapter(heroAdapter);
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_networking, container, false);

        heroList = view.findViewById(R.id.recycler_view);
        heroList.hasFixedSize();
        heroList.setLayoutManager(new LinearLayoutManager(view.getContext()));

        return view;
    }
}
