package com.example.anda.utility;

import com.example.anda.model.HeroAPI;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator
{
    private static Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
            .baseUrl("https://api.opendota.com/")
            .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = retrofitBuilder.build();

    private static HeroAPI heroApi = retrofit.create(HeroAPI.class);

    public static HeroAPI getHeroApi()
    {
        return heroApi;
    }
}

