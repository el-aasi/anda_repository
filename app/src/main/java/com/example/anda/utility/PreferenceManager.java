package com.example.anda.utility;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceManager
{
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor preferencesEditor;
    private Context context;

    private static final String PREFERENCES_NAME = "andaPreferences";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    public PreferenceManager(Context context)
    {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, context.MODE_PRIVATE);
        preferencesEditor = sharedPreferences.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime)
    {
        preferencesEditor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        preferencesEditor.apply();
    }

    public boolean isFirstTimeLaunch()
    {
        return sharedPreferences.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }
}
