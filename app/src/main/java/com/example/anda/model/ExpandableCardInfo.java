package com.example.anda.model;

public class ExpandableCardInfo
{
    private String title;
    private int icon;
    private String info;

    public ExpandableCardInfo(String title, String info, int icon)
    {
        this.icon = icon;
        this.title = title;
        this.info = info;
    }

    public String getTitle()
    {
        return title;
    }

    public String getInfo()
    {
        return info;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public int getIcon()
    {
        return icon;
    }

    public void setIcon(int icon)
    {
        this.icon = icon;
    }

    public void setInfo(String info)
    {
        this.info = info;
    }

}
