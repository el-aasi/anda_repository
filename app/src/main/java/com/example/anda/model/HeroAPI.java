package com.example.anda.model;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface HeroAPI
{
    @GET("api/heroStats")
    Call<List<HeroResponse>> getHeroes();
}
