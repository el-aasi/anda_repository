package com.example.anda.model;

import java.util.ArrayList;

public class Hero
{
    private int id;
    private String localized_name;
    private String primary_attr;
    private String attack_type;
    private String imageURL;
    private ArrayList<String> roles;

    public Hero(int id, String localized_name, String primary_attr, String attack_type, String imageURL, ArrayList<String> roles)
    {
        this.id  = id;
        this.localized_name = localized_name;
        this.primary_attr = primary_attr;
        this.attack_type = attack_type;
        this.imageURL = imageURL;
        this.roles = roles;
    }

    public int getId()
    {
        return id;
    }

    public String getLocalized_name()
    {
        return localized_name;
    }

    public String getPrimary_attr()
    {
        return primary_attr;
    }

    public String getAttack_type()
    {
        return attack_type;
    }

    public String getImageURL()
    {
        return imageURL;
    }

    public ArrayList<String> getRoles()
    {
        return roles;
    }
}
