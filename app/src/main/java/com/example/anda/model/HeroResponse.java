package com.example.anda.model;

import java.util.ArrayList;

public class HeroResponse
{
    private int id;
    private String localized_name;
    private String primary_attr;
    private String attack_type;
    private String icon;
    private ArrayList<String> roles;

    public Hero getHero()
    {
        return new Hero(id, localized_name, primary_attr, attack_type, icon, roles);
    }
}

