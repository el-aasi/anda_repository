package com.example.anda.database;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.anda.model.Contact;
import com.example.anda.model.ContactDAO;

@androidx.room.Database(entities = {Contact.class}, version = 1)
public abstract class Database extends RoomDatabase
{
    private static Database instance;

    public abstract ContactDAO contactDAO();

    public static synchronized Database getInstance(Context context)
    {
        if(instance == null)
        {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    Database.class, "database")
                    .fallbackToDestructiveMigration()
                    .build();
        }

        return instance;
    }
}
