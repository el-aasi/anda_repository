package com.example.anda;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.example.anda.activities.SettingsActivity;
import com.example.anda.activities.WebActivity;
import com.example.anda.fragments.GoogleServicesFragment;
import com.example.anda.fragments.HomeFragment;
import com.example.anda.fragments.LocalStorageFragment;
import com.example.anda.fragments.NetworkingFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{
    private DrawerLayout drawerLayout;

    private final FragmentManager fragmentManager = getSupportFragmentManager();

    private Fragment activeFragment;
    private Fragment homeFragment;
    private Fragment googleServicesFragment;
    private Fragment localStorageFragment;
    private Fragment networkingFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState == null)
        {
            homeFragment = new HomeFragment();
            googleServicesFragment = new GoogleServicesFragment();
            localStorageFragment = new LocalStorageFragment();
            networkingFragment = new NetworkingFragment();

            fragmentManager.beginTransaction().add(R.id.main_container, homeFragment, "home").commit();
            fragmentManager.beginTransaction().add(R.id.main_container, googleServicesFragment, "googleServices").hide(googleServicesFragment).commit();
            fragmentManager.beginTransaction().add(R.id.main_container, localStorageFragment, "localStorage").hide(localStorageFragment).commit();
            fragmentManager.beginTransaction().add(R.id.main_container, networkingFragment, "networking").hide(networkingFragment).commit();
        } else
        {
            homeFragment = getSupportFragmentManager().findFragmentByTag("home");
            googleServicesFragment = getSupportFragmentManager().findFragmentByTag("googleServices");
            localStorageFragment = getSupportFragmentManager().findFragmentByTag("localStorage");
            networkingFragment = getSupportFragmentManager().findFragmentByTag("networking");
        }

        if(activeFragment == null)
        {
            activeFragment = homeFragment;
        }

        Toolbar toolbar = findViewById(R.id.andaApplicationBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Home");

        drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.nav_open, R.string.nav_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.drawer_nav);
        navigationView.setNavigationItemSelectedListener(this);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(item ->
        {
            switch(item.getItemId())
            {
                case R.id.action_button_home:
                    fragmentManager.beginTransaction().hide(activeFragment).show(homeFragment).commit();
                    activeFragment = homeFragment;
                    getSupportActionBar().setTitle("Home");
                    return true;
                case R.id.action_button_google_services:
                    fragmentManager.beginTransaction().hide(activeFragment).show(googleServicesFragment).commit();
                    activeFragment = googleServicesFragment;
                    getSupportActionBar().setTitle("Google Services");
                    return true;
                case R.id.action_button_local_storage:
                    fragmentManager.beginTransaction().hide(activeFragment).show(localStorageFragment).commit();
                    activeFragment = localStorageFragment;
                    getSupportActionBar().setTitle("Local Storage");
                    return true;
                case R.id.action_button_networking:
                    fragmentManager.beginTransaction().hide(activeFragment).show(networkingFragment).commit();
                    activeFragment = networkingFragment;
                    getSupportActionBar().setTitle("Networking");
                    return true;
            }
            return false;
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.actions_toolbar, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem)
    {
        switch(menuItem.getItemId())
        {
            case R.id.drawer_action_settings:
                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                startActivity(settingsIntent);
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.drawer_action_about:
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.action_button_stackoverflow:
                openBrowser();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openBrowser()
    {
        Intent intentWebActivity = new Intent(this, WebActivity.class);
        intentWebActivity.putExtra("link", "https://stackoverflow.com/users/7845728/el-aasi");
        startActivity(intentWebActivity);
    }
}
