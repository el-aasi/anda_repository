package com.example.anda.repository;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.example.anda.database.Database;
import com.example.anda.model.Contact;
import com.example.anda.model.ContactDAO;
import com.example.anda.model.Hero;
import com.example.anda.model.HeroAPI;
import com.example.anda.model.HeroResponse;
import com.example.anda.utility.ServiceGenerator;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repository
{
    private static Repository instance;
    private ContactDAO contactDAO;
    private MutableLiveData<List<Hero>> heroes;
    private LiveData<List<Contact>> allContacts;

    private Repository(Application application)
    {
        Database database = Database.getInstance(application);
        heroes = new MutableLiveData<>();
        contactDAO = database.contactDAO();
        allContacts = contactDAO.getAllContacts();
    }

    public static synchronized Repository getInstance(Application application)
    {
        if(instance == null)
        {
            instance = new Repository(application);
        }

        return instance;
    }

    public LiveData<List<Contact>> getAllContacts()
    {
        return allContacts;
    }

    public void insert(Contact contact)
    {
        new InsertContactsAsync(contactDAO).execute(contact);
    }

    public void deleteAllContacts()
    {
        new DeleteAllContactsAsync(contactDAO).execute();
    }

    private static class InsertContactsAsync extends AsyncTask<Contact, Void, Void>
    {
        private ContactDAO contactDAO;

        private InsertContactsAsync(ContactDAO contactDAO)
        {
            this.contactDAO = contactDAO;
        }

        @Override
        protected Void doInBackground(Contact... contacts)
        {
            contactDAO.insert(contacts[0]);
            return null;
        }
    }

    private static class DeleteAllContactsAsync extends AsyncTask<Void, Void, Void>
    {
        private ContactDAO contactDAO;

        private DeleteAllContactsAsync(ContactDAO contactDAO)
        {
            this.contactDAO = contactDAO;
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            contactDAO.deleteAllContacts();
            return null;
        }
    }

    public LiveData<List<Hero>> getHeroes()
    {
        return heroes;
    }

    public void requestHeroes()
    {
        final HeroAPI heroApi = ServiceGenerator.getHeroApi();
        Call<List<HeroResponse>> call = heroApi.getHeroes();
        call.enqueue(new Callback<List<HeroResponse>>()
        {
            @Override
            public void onResponse(Call<List<HeroResponse>> call, Response<List<HeroResponse>> response)
            {
                if(response.code() == 200)
                {
                    List<Hero> heroList = new ArrayList<>();

                    for(int i = 0; i < response.body().size(); i++)
                    {
                        heroList.add(response.body().get(i).getHero());
                    }
                    heroes.setValue(heroList);
                }
            }

            @Override
            public void onFailure(Call<List<HeroResponse>> call, Throwable t)
            {
                Log.i("Retrofit", "Something went wrong :(");
            }
        });
    }

}
