package com.example.anda.viewmodel;

import android.app.Application;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.anda.R;
import com.example.anda.model.ExpandableCardInfo;

import java.util.ArrayList;

public class CardViewModel extends AndroidViewModel
{
    private ArrayList<ExpandableCardInfo> cardList;

    public CardViewModel(@NonNull Application application)
    {
        super(application);
    }

    private void CreateList()
    {
        cardList = new ArrayList<>();

        cardList.add(new ExpandableCardInfo("Layout", "The layouts are used through the entire application." +
                "From base layouts for activities and fragments to small container layouts.", R.drawable.ic_view_column_black_24dp));
        cardList.add(new ExpandableCardInfo("Activities and Resources", "Even if the initial " +
                "intention was to create single activity app, this application still contains several activities.", R.drawable.ic_present_to_all_black_24dp));
        cardList.add(new ExpandableCardInfo("Intents and Fragments", "Fragments are at the core of this app. It is " +
                "formed by four main fragments that present the general functionality needed.", R.drawable.ic_format_paint_black_24dp));
        cardList.add(new ExpandableCardInfo("User Experience", "When developing this app, the " +
                "use experience was kept in mind. The idea was to makes it simple to use and understand.", R.drawable.ic_person_black_24dp));
        cardList.add(new ExpandableCardInfo("Recycler Views", "Recycler views are used to provide" +
                "smooth presentation of diverse lists int he app.", R.drawable.ic_format_list_bulleted_black_24dp));
        cardList.add(new ExpandableCardInfo("Application Architecture", "MVVM architecture is present in some parts of " +
                "the application. It provides a proper separation of responsibilities and control of the application. ", R.drawable.ic_apps_black_24dp));
        cardList.add(new ExpandableCardInfo("Local Data Storage", "Local storage is used to " +
                "store a list of contacts in the local storage of the application.", R.drawable.ic_sd_storage_black_24dp));
        cardList.add(new ExpandableCardInfo("Networking", "A call to an API is used to retrieve the list of " +
                "Dota2 Heroes. ", R.drawable.ic_network_check_black_24dp));
        cardList.add(new ExpandableCardInfo("Google Services", "A google service chosen for this app is " +
                "Google Maps.", R.drawable.ic_verified_user_black_24dp));
        cardList.add(new ExpandableCardInfo("Testing", "Not fully extensive, but still present unit testing" +
                "of the application is made, using various external libraries.", R.drawable.ic_assignment_turned_in_black_24dp));
        cardList.add(new ExpandableCardInfo("Additional", "The application uses several external libraries for both" +
                "internal features and testing.", R.drawable.ic_attachment_black_24dp));
    }

    public ArrayList<ExpandableCardInfo> getAllCards()
    {
        CreateList();
        return cardList;
    }
}
