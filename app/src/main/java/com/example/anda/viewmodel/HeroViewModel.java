package com.example.anda.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.anda.model.Hero;
import com.example.anda.repository.Repository;

import java.util.List;

public class HeroViewModel extends AndroidViewModel
{
    Repository repository;

    public HeroViewModel(@NonNull Application application)
    {
        super(application);
        repository = Repository.getInstance(application);
    }

    public LiveData<List<Hero>> getHeroes()
    {
        repository.requestHeroes();
        return repository.getHeroes();
    }
}
