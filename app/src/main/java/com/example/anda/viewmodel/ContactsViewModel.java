package com.example.anda.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.anda.model.Contact;
import com.example.anda.repository.Repository;

import java.util.List;

public class ContactsViewModel extends AndroidViewModel
{

    private Repository repository;

    public ContactsViewModel(@NonNull Application application)
    {
        super(application);
        repository = Repository.getInstance(application);
    }

    public LiveData<List<Contact>> getAllContacts()
    {
        return repository.getAllContacts();
    }

    public void insert(final Contact contact)
    {
        repository.insert(contact);
    }

    public void deleteAllContacts()
    {
        repository.deleteAllContacts();
    }
}
