package com.example.anda.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import androidx.core.text.HtmlCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.anda.MainActivity;
import com.example.anda.utility.PreferenceManager;
import com.example.anda.R;

public class WelcomeActivity extends AppCompatActivity
{
    private ViewPager viewPager;
    private CustomViewPagerAdapter customViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] sliderDots;
    private int[] layouts;
    private Button buttonSkip, buttonNext;
    private PreferenceManager preferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        preferenceManager = new PreferenceManager(this);
        if(!preferenceManager.isFirstTimeLaunch())
        {
            launchMainActivity();
            finish();
        }

        if(Build.VERSION.SDK_INT >= 21)
        {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(R.layout.activity_welcome);

        viewPager = findViewById(R.id.view_pager);
        dotsLayout = findViewById(R.id.layoutDots);
        buttonSkip = findViewById(R.id.btn_skip);
        buttonNext = findViewById(R.id.btn_next);

        layouts = new int[]{
                R.layout.welcome_screen1,
                R.layout.welcome_screen2,
                R.layout.welcome_screen3,
                R.layout.welcome_screen4};

        addSliderDots(0);

        changeStatusBarColor();

        customViewPagerAdapter = new CustomViewPagerAdapter();
        viewPager.setAdapter(customViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        buttonSkip.setOnClickListener(x -> launchMainActivity());

        buttonNext.setOnClickListener(x ->
        {
            int current = getViewPagerItem(+1);
            if(current < layouts.length)
            {
                viewPager.setCurrentItem(current);
            } else
            {
                launchMainActivity();
            }
        });
    }

    private void addSliderDots(int currentPage)
    {
        sliderDots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.welcome_array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.welcome_array_dot_inactive);

        dotsLayout.removeAllViews();

        for(int i = 0; i < sliderDots.length; i++)
        {
            sliderDots[i] = new TextView(this);
            sliderDots[i].setText(HtmlCompat.fromHtml("&#8226;", HtmlCompat.FROM_HTML_MODE_LEGACY));
            sliderDots[i].setTextSize(35);
            sliderDots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(sliderDots[i]);
        }

        if(sliderDots.length > 0)
            sliderDots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private int getViewPagerItem(int i)
    {
        return viewPager.getCurrentItem() + i;
    }

    private void launchMainActivity()
    {
        preferenceManager.setFirstTimeLaunch(false);
        startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
        finish();
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener()
    {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
        {

        }

        @Override
        public void onPageSelected(int position)
        {
            addSliderDots(position);

            if(position == layouts.length - 1)
            {
                buttonNext.setText(getString(R.string.start));
                buttonSkip.setVisibility(View.GONE);
            } else
            {
                // still pages are left
                buttonNext.setText(getString(R.string.next));
                buttonSkip.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state)
        {
        }
    };


    private void changeStatusBarColor()
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public class CustomViewPagerAdapter extends PagerAdapter
    {
        private LayoutInflater layoutInflater;

        public CustomViewPagerAdapter()
        {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount()
        {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj)
        {
            return view == obj;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object)
        {
            View view = (View) object;
            container.removeView(view);
        }
    }
}
