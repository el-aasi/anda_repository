package com.example.anda.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreferenceCompat;

import com.example.anda.utility.PreferenceManager;
import com.example.anda.R;

public class SettingsActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();

        Toolbar toolbar = findViewById(R.id.andaApplicationBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Settings");
    }

    public static class SettingsFragment extends PreferenceFragmentCompat
    {
        PreferenceManager preferenceManager;
        SwitchPreferenceCompat firstLaunchSwitch;

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
        {
            preferenceManager = new PreferenceManager(this.getContext());
            setPreferencesFromResource(R.xml.root_preferences, rootKey);

            //Welcome Slider Option
            firstLaunchSwitch = findPreference("welcome_slider");
            firstLaunchSwitch.setChecked(preferenceManager.isFirstTimeLaunch());
            if(firstLaunchSwitch != null)
            {
                firstLaunchSwitch.setOnPreferenceChangeListener((preference, newValue) ->
                {
                    if(firstLaunchSwitch.isChecked())
                    {
                        preferenceManager.setFirstTimeLaunch(false);
                    }
                    else
                    {
                        preferenceManager.setFirstTimeLaunch(true);
                    }

                    firstLaunchSwitch.setChecked(preferenceManager.isFirstTimeLaunch());
                    return false;
                });
            }
        }
    }
}