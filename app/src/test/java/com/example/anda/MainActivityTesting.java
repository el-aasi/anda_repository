package com.example.anda;

import android.content.Intent;
import android.os.Build;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.test.core.app.ActivityScenario;

import com.example.anda.activities.SettingsActivity;
import com.example.anda.activities.WebActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.annotation.LooperMode;
import org.robolectric.fakes.RoboMenuItem;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowLooper;

import java.util.concurrent.TimeUnit;

import static org.robolectric.Shadows.shadowOf;
import static org.robolectric.annotation.LooperMode.Mode.PAUSED;
import static org.assertj.core.api.Assertions.*;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = Build.VERSION_CODES.O_MR1)
@LooperMode(PAUSED)
public class MainActivityTesting
{
    private ActivityScenario<MainActivity> mainActivityActivityScenario;

    @Before
    public void setUp()
    {
        mainActivityActivityScenario = ActivityScenario.launch(MainActivity.class);
    }

    @After
    public void tearDow()
    {
        mainActivityActivityScenario.close();
    }

    @Test
    public void MainActivityTest_Create()
    {
        assertThat(mainActivityActivityScenario).isNotNull();

        mainActivityActivityScenario.onActivity(
                activity -> assertThat(activity.getLifecycle().getCurrentState()).isEqualTo(Lifecycle.State.RESUMED));

        mainActivityActivityScenario.moveToState(Lifecycle.State.STARTED);
        mainActivityActivityScenario.onActivity(
                activity -> assertThat(activity.getLifecycle().getCurrentState()).isEqualTo(Lifecycle.State.STARTED));

        mainActivityActivityScenario.moveToState(Lifecycle.State.CREATED);
        mainActivityActivityScenario.onActivity(
                activity -> assertThat(activity.getLifecycle().getCurrentState()).isEqualTo(Lifecycle.State.CREATED));
    }

    @Test
    public void MainActivityTest_pressStackOverflowButton()
    {
        mainActivityActivityScenario.onActivity(activity ->
        {
            activity.findViewById(R.id.action_button_stackoverflow).performClick();

            ShadowActivity mainActivityShadow = shadowOf(activity);
            Intent stackOverflowIntent = mainActivityShadow.getNextStartedActivity();

            assertThat(stackOverflowIntent.getComponent().getClassName()).isEqualTo(WebActivity.class.getName());
            assertThat(stackOverflowIntent.getStringExtra("link")).isEqualTo("https://stackoverflow.com/users/7845728/el-aasi");
        });
    }

    @Test
    public void MainActivityTest_pressedSettingsButton()
    {
        mainActivityActivityScenario.onActivity(activity ->
        {
            activity.onNavigationItemSelected(new RoboMenuItem(R.id.drawer_action_settings));

            ShadowActivity mainActivityShadow = shadowOf(activity);
            Intent settingsIntent = mainActivityShadow.getNextStartedActivity();

            assertThat(settingsIntent.getComponent().getClassName()).isEqualTo(SettingsActivity.class.getName());
        });
    }

    @Test
    public void MainActivityTest_checkIfFragmentsCreatedOnLaunchAndHomeFragmentNotHidden()
    {
        mainActivityActivityScenario.onActivity(activity ->
        {
            Fragment homeFragment = activity.getSupportFragmentManager().findFragmentByTag("home");
            Fragment googleFragment = activity.getSupportFragmentManager().findFragmentByTag("googleServices");
            Fragment localStorageFragment = activity.getSupportFragmentManager().findFragmentByTag("localStorage");
            Fragment networkingFragment = activity.getSupportFragmentManager().findFragmentByTag("networking");

            assertThat(homeFragment).isNotNull();
            assertThat(googleFragment).isNotNull();
            assertThat(localStorageFragment).isNotNull();
            assertThat(networkingFragment).isNotNull();

            assertThat(homeFragment.isAdded()).isTrue();
            assertThat(googleFragment.isAdded()).isTrue();
            assertThat(localStorageFragment.isAdded()).isTrue();
            assertThat(networkingFragment.isAdded()).isTrue();

            assertThat(homeFragment.isHidden()).isFalse();
            assertThat(googleFragment.isHidden()).isTrue();
            assertThat(localStorageFragment.isHidden()).isTrue();
            assertThat(networkingFragment.isHidden()).isTrue();
        });
    }

    @Test
    public void MainActivityTest_checkIfFragmentIsNotHiddenWhenButtonPressedAndOtherFragmentsHidden()
    {
        mainActivityActivityScenario.onActivity(activity ->
        {
            Fragment homeFragment = activity.getSupportFragmentManager().findFragmentByTag("home");
            Fragment googleFragment = activity.getSupportFragmentManager().findFragmentByTag("googleServices");
            Fragment localStorageFragment = activity.getSupportFragmentManager().findFragmentByTag("localStorage");
            Fragment networkingFragment = activity.getSupportFragmentManager().findFragmentByTag("networking");

            BottomNavigationView bottomNavigationView = activity.findViewById(R.id.bottom_navigation);
            bottomNavigationView.findViewById(R.id.action_button_google_services).performClick();

            ShadowLooper.idleMainLooper(1000, TimeUnit.MILLISECONDS);

            assertThat(homeFragment.isHidden()).isTrue();
            assertThat(googleFragment.isHidden()).isFalse();
            assertThat(localStorageFragment.isHidden()).isTrue();
            assertThat(networkingFragment.isHidden()).isTrue();

            bottomNavigationView.findViewById(R.id.action_button_local_storage).performClick();

            ShadowLooper.idleMainLooper(1000, TimeUnit.MILLISECONDS);

            assertThat(homeFragment.isHidden()).isTrue();
            assertThat(googleFragment.isHidden()).isTrue();
            assertThat(localStorageFragment.isHidden()).isFalse();
            assertThat(networkingFragment.isHidden()).isTrue();

            bottomNavigationView.findViewById(R.id.action_button_networking).performClick();

            ShadowLooper.idleMainLooper(1000, TimeUnit.MILLISECONDS);

            assertThat(homeFragment.isHidden()).isTrue();
            assertThat(googleFragment.isHidden()).isTrue();
            assertThat(localStorageFragment.isHidden()).isTrue();
            assertThat(networkingFragment.isHidden()).isFalse();

            bottomNavigationView.findViewById(R.id.action_button_home).performClick();

            ShadowLooper.idleMainLooper(1000, TimeUnit.MILLISECONDS);

            assertThat(homeFragment.isHidden()).isFalse();
            assertThat(googleFragment.isHidden()).isTrue();
            assertThat(localStorageFragment.isHidden()).isTrue();
            assertThat(networkingFragment.isHidden()).isTrue();
        });
    }

}
